package part2_AvgHomeworkScore;

public class HomeworkScore {
	private String name;
	private double score;
	private double sum;

	public HomeworkScore(String name){
		this.name = name;
		this.score = 0;
	}
	
	public void setScore(double score){
		this.score += score;
	}
	
	public double getScore(){
		return score;
	}
	
	public String getName(){
		return name;
	}
}
