package part2_AvgHomeworkScore;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;


public class HomeworkMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String filename = "Homework.txt";
		ArrayList<HomeworkScore> list = new ArrayList<HomeworkScore>();
		FileReader fileReader = null;
		
		try {
			fileReader = new FileReader(filename);
			BufferedReader buffer = new BufferedReader(fileReader);
			String line;
			for (line = buffer.readLine(); line != null; line = buffer.readLine()){
				String[] data = line.split(", ");
				String name = data[0].trim();
				Double s1 = Double.parseDouble(data[1].trim());
				Double s2 = Double.parseDouble(data[2].trim());
				Double s3 = Double.parseDouble(data[3].trim());
				Double s4 = Double.parseDouble(data[4].trim());
				Double s5 = Double.parseDouble(data[5].trim());
				double sum = s1+s2+s3+s4+s5;
				
				HomeworkScore h = new HomeworkScore(name);
				h.setScore(sum);
				list.add(h);
				
			}
			
			FileWriter fileWriter = null;
			fileWriter = new FileWriter("average.txt");
			BufferedWriter out = new BufferedWriter(fileWriter);
			
			System.out.println("--------- Homework Scores ---------");
			System.out.println("Name \t   Average");
			System.out.println("====\t============");

			for(HomeworkScore i : list){
				System.out.println(i.getName()+"        "+i.getScore()/5);
				out.write(i.getName()+" "+i.getScore()/5);
			}
			out.flush();
		
		}
		catch (FileNotFoundException e){
			System.err.println("Cannot read file "+filename);
		}
		catch (IOException e){
			System.err.println("Error reading from file");
		}
		finally {
			try {
				if (fileReader != null)
						fileReader.close();
			} catch (IOException e) {
				System.err.println("Error closing files");
			}
	}
	}

}
