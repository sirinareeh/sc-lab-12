package part1_PhoneBook;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class PhoneBookMain {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String filename = "PhoneBook.txt";
		ArrayList<PhoneBook> list = new ArrayList<PhoneBook>();

		try {
			
			FileReader fileReader = new FileReader(filename);
			BufferedReader buffer = new BufferedReader(fileReader);

			String line;
			for (line = buffer.readLine(); line != null; line = buffer.readLine()){
				String[] data = line.split(", ");
				String name = data[0].trim();
				String phoneNum = data[1].trim();

				PhoneBook p = new PhoneBook(name,phoneNum);
				list.add(p);		
			}
				System.out.println("--------- Java Phone Book ---------");
				System.out.println("Name \t   Phone");
				System.out.println("====\t============");
			
			for(PhoneBook i: list){
				System.out.println(i.toString());
			}
			
			
		}
		catch (FileNotFoundException e){
			System.err.println("Cannot read file "+filename);
			}
		
		catch (IOException e){
			System.err.println("Error reading from user");
			}
		
	}
}
