package part1_PhoneBook;

public class PhoneBook {
	private String name;
	private String phoneNum;
	
	public PhoneBook(String name, String phoneNum){
		this.name = name;
		this.phoneNum = phoneNum;	
	}
	
	public String toString(){
		return name+",\t"+phoneNum;
	}
}

